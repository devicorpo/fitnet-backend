const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
dotenv.config();

//import Routes
const sectionRouter = require("./routes/section");
const videoRouter = require("./routes/video");
//Middlewares
app.use(express.json());
app.use(bodyParser.json());
//Route middlewares
app.use("/api/public/section", sectionRouter);
app.use("/api/public/section/video", videoRouter);

//connect to db
mongoose.connect(process.env.DB_CONNECT, () => console.log("connected to db"));
let port = process.env.PORT;
if (port === null || port === "") {
  port = 3000;
}
app.listen(3000, () => console.log("Server up and running"));
