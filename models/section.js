const mongoose = require("mongoose");
const video = require("./video");
const sectionSchema = new mongoose.Schema({
  instructor_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "instructor",
  },
  title: {
    type: String,
    required: true,
    max: 255,
  },
  description: {
    type: String,
    require: true,
  },
  date: {
    type: Date,
    default: Date.now(),
  },
});
module.exports = mongoose.model("Section", sectionSchema);
