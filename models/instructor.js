const mongoose = require("mongoose");
const instructorSchema = mongoose.Schema({
  first_name: {
    type: String,
    require: true,
    max: 20,
  },
  last_name: {
    type: String,
    require: true,
    max: 20,
  },
  skills: {
    type: [String],
    require: true,
  },
});
