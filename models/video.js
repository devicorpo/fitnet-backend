const mongoose = require("mongoose");
const videoSchema = new mongoose.Schema({
  section_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "section",
  },
  title: {
    type: String,
    required: true,
    max: 255,
  },
  description: {
    type: String,
    required: true,
  },
  url: {
    type: String,
    required: true,
  },
  timeline: {
    type: String,
    required: true,
  },
  completedBy: {
    type: [String],
  },
  date: {
    type: Date,
    default: Date.now(),
  },
});
module.exports = mongoose.model("Video", videoSchema);
