const router = require("express").Router();
const uuid = require("uuid");
const Video = require("../models/video");
const Section = require("../models/section");

//getOne section
router.get("/getOne/sectionId/:sectionId", async (req, res) => {
  const sectionExits = await Section.findById({ _id: req.params.sectionId });
  if (!sectionExits) return res.status(400).send("section doesn't exits");
  res.json(sectionExits);
});

//getOne section
router.get("/getAll/instructorId/:instructor_id", async (req, res) => {
  const sectionExits = await Section.find({
    instructor_id: req.params.instructor_id,
  });
  if (!sectionExits) return res.status(400).send("user doesn't exits");
  res.json(sectionExits);
});

//make new section
router.post("/create", async (req, res) => {
  const section = new Section({
    instructor_id: req.body.instructor_id,
    title: req.body.section_title,
    description: req.body.section_description,
  });
  try {
    const savedSection = await section.save();
    res.send(savedSection);
  } catch (error) {
    res.status(400).send(error);
  }
});

module.exports = router;

//Update video in the section
// router.patch("/section/file/create/video", async (req, res) => {
//   const sectionExits = await Section.findOne({ _id: req.body.section_id });
//   if (!sectionExits) return res.status(400).send("section doesn't exits");
//   const video = new Video({
//     video_title: req.body.video_title,
//     video_description: req.body.video_description,
//     video_url: req.body.video_url,
//     video_timeline: req.body.video_timeline,
//   });
//   video.validate((err) => {
//     if (err) return res.send(err);
//   });
//   sectionExits.videos.push(video);
//   const videos = sectionExits.videos;
//   try {
//     const updateSection = await Section.updateOne(
//       { id: req.body.section_id },
//       { $push: { videos: video } }
//     );
//     res.json(updateSection);
//   } catch (error) {
//     res.status(400).send(error);
//   }
// });
