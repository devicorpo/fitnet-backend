const router = require("express").Router();
const Video = require("../models/video");
const Section = require("../models/video");

router.get("/getOne/videoId/:video_id", async (req, res) => {
  const videoExits = await Video.findById({ _id: req.params.video_id });
  if (!videoExits) return res.status(400).send("Video doesn't exits");
  res.json(videoExits);
});
//getOne section
router.get("/getAll/sectionId/:section_id", async (req, res) => {
  const sectionExits = await Video.find({
    section_id: req.params.section_id,
  });
  if (!sectionExits) return res.status(400).send("section doesn't exits");
  res.json(sectionExits);
});
router.post("/create", async (req, res) => {
  // const sectionExits = await Section.findById({ _id: req.body.section_id });
  // if (!sectionExits) return res.status(400).send("section doesn't exits");
  const video = new Video({
    section_id: req.body.section_id,
    title: req.body.video_title,
    description: req.body.video_description,
    url: req.body.video_url,
    timeline: req.body.video_timeline,
  });

  try {
    const saveVideo = await video.save();
    res.json(saveVideo);
  } catch (error) {
    res.status(400).send(error);
  }
});

// update user who complete the video
router.patch("/update/completedBy", async (req, res) => {
  const taine_id = req.body.taine_id;
  await Video.findByIdAndUpdate(
    req.body.video_id,
    {
      $push: { completedBy: taine_id },
    },
    (err, doc) => {
      if (err) return handleError(err);
      res.send(doc);
    }
  );
});
module.exports = router;
